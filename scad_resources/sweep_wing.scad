
use <naca/Naca4.scad>
use <naca/Naca_sweep.scad>



module half_wing(camber=0, camber_pos=0, thickess=10, half_span = 50, chord0=10, chordf=1, curve=0, dz=1)
{
    
    
    function gen_dat(M,dz,N) = [for (i=[0:dz:M])
        
        let( af = vec3D(
            airfoil_data([camber_func(i), camberpos_func(i), thickness_func(i)], L=chord(i), N=N)))
        let( af=T_(chord0-chord(i),height(i),i,af))
        //let( af=Rx_(rotation(i),af))
        af];
    
    function camberpos_func(i) = camber_pos/100;
    function camber_func(i) = camber/100;
    function thickness_func(i) = thickness/100;
    function chord(i) = chord0*(1-exp((i-half_span)/3));
    function height(i) = curve*exp((i-half_span)/(half_span/6));
    function rotation(i) = curve*exp((i-half_span)/5);
    //echo(rotation(half_span));
    //echo(gen_dat(M=half_span,dz=1,N=80));
    sweep(gen_dat(M=half_span, dz=dz, N=80), showslices=false, plaincaps=true, convexity=10);
    }


module full_wing(camber=0, camber_pos =0, thickness=10, span = 60, chord0=10, chordf=1, curve=10, dz=.1)
{

    rotate([-90, 0, -90]) half_wing(camber=camber, thickness=thickness, half_span=span/2, chord0=chord0,chordf=chordf, curve=curve, dz=dz);
    mirror([1,0,0]) rotate([-90, 0, -90]) half_wing(camber=camber, thickness=thickness, half_span=span/2, chord0=chord0,chordf=chordf, curve=curve, dz=dz);
    
}


full_wing();

