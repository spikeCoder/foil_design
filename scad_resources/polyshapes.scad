/* Skip to content
Personal Open source Business Explore
Sign upSign inPricingBlogSupport
This repository
Search
 Watch 10  Star 34  Fork 7 KitWallace/openscad
 Code  Issues 0  Pull requests 0  Projects 0  Pulse  Graphs
Branch: master Find file Copy pathopenscad/poly_surface.scad
6430393  on Feb 24, 2015
@KitWallace KitWallace Create poly_surface.scad
1 contributor
RawBlameHistory    
163 lines (135 sloc)  3.52 KB
/*
    computed surface construction  by  Kit Wallace
    
    Code licensed under the Creative Commons - Attribution - Share Alike license.

   The project is being documented in my blog 
     http://kitwallace.tumblr.com/tagged/openscad
  
*/

function flatten(l) = 
// remove one level of brackets
    [ for (a = l) for (b = a) b ] ;
function reverse(l) = 
//  reverse the elements of an array
    [ for (i=[1:len(l)]) l[len(l)-i]];   

// vertex indexes
function vt(i,j,nx,ny) = i*nx + j;
function vb(i,j,nx,ny) = nx*ny +i * nx + j;
  
function surface_vertices (minx,maxx,miny,maxy,nx,ny) = 
// vertices 
    concat(
     flatten([for (i=[0:nx-1])
        let (x= minx + (maxx-minx)*i/nx)
        [for (j=[0:ny-1])
            let (y= miny + (maxy-miny)*j/ny)
            [x,y,ftop(x,y)]
        ]
    ]),
    flatten([for (i=[0:nx-1])
       let (x= minx + (maxx-minx)*i/nx)
       [for (j=[0:ny-1])
           let (y= miny + (maxy-miny)*j/ny)
           [x,y,fbottom(x,y)]
        ]
    ])
    );
        
function surface_faces (nx,ny) = 
  concat(
    flatten(
      [for (i=[0:nx-2])
        [for (j=[0:ny-2])
           reverse(
            [vt(i,j,nx,nx),
            vt(i+1,j,nx,nx),
            vt(i+1,j+1,nx,nx),
            vt(i,j+1,nx,nx)]
           )
        ]
     ]),
    flatten(
      [for (i=[0:nx-2])
         [for (j=[0:ny-2])
             [vb(i,j,nx,nx),
              vb(i+1,j,nx,nx),
              vb(i+1,j+1,nx,nx),
              vb(i,j+1,nx,nx)
             ]
         ]
       ]),
    [for (i=[0:nx-2])
           [vt(i,0,nx,nx),
            vt(i+1,0,nx,nx),
            vb(i+1,0,,nx,nx),
            vb(i,0,,nx,nx)
           ]
    ],
    [for (i=[0:nx-2])
        reverse(
           [vt(i,ny-1,nx,nx),
            vt(i+1,ny-1,nx,nx),
            vb(i+1,ny-1,nx,nx),
            vb(i,ny-1,nx,nx)
           ])
    ],
    [for (j=[0:ny-2])
       reverse(
         [vt(0,j,nx,nx),
         vt(0,j+1,nx,nx),
         vb(0,j+1,nx,nx),
         vb(0,j,nx,nx)])
    ],
     [for (j=[0:ny-2])
        [vt(nx-1,j,nx,nx),
         vt(nx-1,j+1,nx,nx),
         vb(nx-1,j+1,nx,nx),
         vb(nx-1,j,nx,nx)]
       ]   
    );


module poly_surface (minx,maxx,miny,maxy,nx,ny) {
// uses ftop() and fbottom()  to computer surface heights 
   sv=surface_vertices(minx,maxx,miny,maxy,nx,ny);   
   // echo("vertices",sv);
   sf=surface_faces(nx,ny);     
   // echo("faces",sf);    
   polyhedron(sv,sf);
};
  
module ruler(n) {
    color([0,0,1,0.5])
    for (i=[0:n-1]) 
       translate([(i-n/2 +0.5)* 10,0,0]) cube([9.8,5,2], center=true);
}

module ground(z=200) {
    translate([0,0,-z]) cube(z*2,center=true);
} 

module sky(height,z=200) {
   translate([0,0,height])
      rotate([0,180,0]) ground(z);
}

// example uses 

/*
function ftop(x,y) = sin(60*x)*sin(90*y) + 2 ;
function fbottom(x,y) =ftop(x,y)-0.5 ;

color("pink") 
    scale([5,5,2.5]) poly_surface(-4,4,-4,4,100,100);  


*/

/*    
function fsinc(x,y) =
// eps eliminates a hole at the peak
     let (eps=0.001,r = sqrt(x*x + y*y + eps))
     sin(180*r)/r;
    
function ftop(x,y) = fsinc(x,y);
function fbottom(x,y) = ftop(x,y)-0.2 ;

color("forestgreen") 
   scale([5,5,2.5]) poly_surface(-4,4,-4,4,100,100);  

//*/
//function fquartic(x,y,a,b) = a*x*x-b*y*y;
function fquartic(x,y) = x*x+y*y;
function fbottom(x,y) = fquartic(x,y);
function ftop(x,y) = 10;
//scale([1,1,1]) poly_surface(-1,1,-1,1,100,100);



module quad_cone(height=1, radius=1, FN = 20)
{
height0 = 1;
scale([radius, radius, height]) intersection() 
    {
    
    color("forestgreen") 
    translate([0, 0, height0/2]) cylinder (h = height0, r=1, center = true, $fn=FN);
    poly_surface(-2,2,-2,2,FN,FN);
    };
    
    
};


quad_cone();


/*
function fquartic(x,y,a,b) = a*x*x-b*y*y;
function ftop(x,y) = fquartic(x,y,1,1)+20;
function fbottom(x,y) = 0 ;

color("cornsilk") 
 scale([5,5,0.5]) poly_surface(-4,4,-4,4,100,100);  
