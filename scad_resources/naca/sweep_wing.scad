
use <naca/Naca4.scad>
use <naca/Naca_sweep.scad>


module sweep_wing()
{
    function gen_dat(M,dz,N) = [for (i=[1:dz:M])
        let( L = length(i))
        let( af = vec3D(
        airfoil_data([0, 0, thickness(i)], L=length(i), N=N)))
        T_(0, 0, i,R_(0,i,0,af))];  // ranslate airfoil
    
    
    function thickness(i) = 10;
    function length(i) = 10;
    sweep(gen_dat(M=100,dz=0.1,N=100));
};


sweep_wing();