#!/bin/bash
# based on install instructions at https://openfoamwiki.net/index.php/Installation/Linux/OpenFOAM-2.4.0/Ubuntu

$STARTINGDIR = $(pwd)

# install required packages
sudo apt-get update
sudo apt-get install build-essential binutils-dev cmake flex bison zlib1g-dev qt4-dev-tools libqt4-dev libqtwebkit-dev gnuplot \
libreadline-dev libncurses-dev libxt-dev libopenmpi-dev openmpi-bin libboost-system-dev libboost-thread-dev libgmp-dev \
libmpfr-dev python python-dev libcgal-dev

sudo apt-get install libglu1-mesa-dev libqt4-opengl-dev



# download openfoam and third pary dirs
cd ~
mkdir OpenFOAM
cd OpenFOAM

if [ -f "OpenFOAM-2.4.0.tgz" ]
then
	echo "OpenFoam-2.4.0.tgz Found, not downloading"
else
	wget "http://downloads.sourceforge.net/foam/OpenFOAM-2.4.0.tgz?use_mirror=mesh" -O OpenFOAM-2.4.0.tgz
fi
if [ -d "OpenFOAM-2.4.0" ]
	then 
	echo "OpenFoam-2.4.0 found, not extracting"
	else
	tar -xzf OpenFOAM-2.4.0.tgz 
fi
if [ -f "ThirdParty-2.4.0.tgz" ]
	then
		echo "ThirdParty-2.4.0.tgz Found, not downloading"
	else
		wget "http://downloads.sourceforge.net/foam/ThirdParty-2.4.0.tgz?use_mirror=mesh" -O ThirdParty-2.4.0.tgz
fi
if [ -d "ThirdParty-2.4.0" ]
	then
		echo "ThirdParty-2.4.0 Found, not extracting"
	else
		tar -xzf ThirdParty-2.4.0.tgz
fi



#Optional: Let's make a few symbolic links that should ensure that the correct global MPI installation is used by this OpenFOAM installation:
ln -s /usr/bin/mpicc.openmpi OpenFOAM-2.4.0/bin/mpicc
ln -s /usr/bin/mpirun.openmpi OpenFOAM-2.4.0/bin/mpirun

# #Since we are going to use CGAL 4.7 that comes with Ubuntu, we are going to configure accordingly by running:
sed -i -e 's/^\(cgal_version=\).*/\1cgal-system/' OpenFOAM-2.4.0/etc/config/CGAL.sh


# # fpr x86
source $HOME/OpenFOAM/OpenFOAM-2.4.0/etc/bashrc WM_NCOMPPROCS=2

# Save an alias in the personal .bashrc file, simply by running the following command:
echo "alias of240='source \$HOME/OpenFOAM/OpenFOAM-2.4.0/etc/bashrc $FOAM_SETTINGS'" >> $HOME/.bashrc

##### build paraview
cd $WM_THIRD_PARTY_DIR
export QT_SELECT=qt4

#Then we need to do several fixes:
sed -i -e 's=//#define GLX_GLXEXT_LEGACY=#define GLX_GLXEXT_LEGACY=' \
  ParaView-4.1.0/VTK/Rendering/OpenGL/vtkXOpenGLRenderWindow.cxx
 
cd $WM_THIRD_PARTY_DIR/ParaView-4.1.0
 
wget http://www.paraview.org/pipermail/paraview/attachments/20140210/464496cc/attachment.bin -O Fix.patch
patch -p1 < Fix.patch
 
cd VTK
wget https://github.com/gladk/VTK/commit/ef22d3d69421581b33bc0cd94b647da73b61ba96.patch -O Fix2.patch
patch -p1 < Fix2.patch
 
cd ../..

# For building ParaView with Python and MPI, it depends on whether you have installed the i686 or x86_64 architecture of Ubuntu. To check this, run:
#For x86_64:
#this will take a while... somewhere between 30 minutes to 2 hours or more
./makeParaView4 -python -mpi -python-lib /usr/lib/x86_64-linux-gnu/libpython2.7.so.1.0 > log.makePV 2>&1

# Finally, update the shell environment:
wmSET $FOAM_SETTINGS



#Now, before we can build OpenFOAM, we need to do a few fixes:
#Go into OpenFOAM's main source folder
cd $WM_PROJECT_DIR
echo $(pwd)
#Change how the flex version is checked
find src applications -name "*.L" -type f | xargs sed -i -e 's=\(YY\_FLEX\_SUBMINOR\_VERSION\)=YY_FLEX_MINOR_VERSION < 6 \&\& \1='

#Now, let's simply create empty folders for the Boost and CGAL installation paths:
mkdir -p $CGAL_ARCH_PATH
mkdir -p $BOOST_ARCH_PATH

#Go into OpenFOAM's main source folder
cd $WM_PROJECT_DIR
 
#Still better be certain that the correct Qt version is being used
export QT_SELECT=qt4
 
# This next command will take a while... somewhere between 30 minutes to 3-6 hours.
./Allwmake > log.make 2>&1
 
#Run it a second time for getting a summary of the installation
./Allwmake > log.make 2>&1