use <scad_resources/naca/Naca4.scad>
use <scad_resources/naca/Naca_sweep.scad>
use <scad_resources/sweep_wing.scad>
use <scad_resources/polyshapes.scad>

// parameters in centemeters, degrees


// postion is relative to forward bottom of mast profile
fusiloge_position = [0, 15, 1.5];
fw_position = [0, 15, 0];
bw_position = [0, -40, 2.7];

mast_height = 125; 
mast_chord = 11.18;
mast_thickness = 1.09; 
mast_thickness_percent=100*mast_thickness/mast_chord;

fusiloge_radius = 1.5;
fusiloge_main_length = 60;
fusiloge_tail_length = 20;
fusiloge_nose_length = 10;

fw_span = 70.0; 
fw_chord = 12;
fw_aoa = 0;
fw_thickness = 1.2;
fw_thickness_percent = 100*fw_thickness/fw_chord;
fw_curve = 8;

bw_span = 40.0;
bw_chord = 6;
bw_aoa = -2;
bw_curve = -1;
bw_thickness = .75;
bw_thickness_percent=100*bw_thickness/bw_chord;



sweep_dz = .5; // sweep resultion size (smaller is higher res)
FN = 50; // cylinder resultion (higher is higher res)



module render_foil()
{
    union()
    {
   translate(fusiloge_position) color("Silver",1) fusiloge();
   color("Teal") mast();
   translate(fw_position) 	color("Black") front_wing();
   translate(bw_position)    color("Black") back_wing();
    }
};

module render_components()
{
translate([100, 0]) render_fusiloge();
translate([100, 0, -5]) render_front_wing();
translate([100, 0, 5]) render_back_wing();
translate([100, 0, 5]) mast();
};

module render_fusiloge()
{ 
    difference() {
		translate(fusiloge_position) fusiloge();
	    mast();
		translate(fw_position) front_wing();
		translate(bw_position) back_wing();
        };
        
};
	

module render_front_wing()
{ 
	translate(fw_position) front_wing();
};

module render_back_wing()
{ 
	translate(bw_position) back_wing();
};



module fusiloge()
{

	

	rotate(a=[90, 0, 0]) cylinder(r = fusiloge_radius, h = fusiloge_main_length, center = false, $fn=FN);

	
	translate([0, fusiloge_nose_length]) rotate(a=[90, 0, 0]) quad_cone(height=fusiloge_nose_length, radius=fusiloge_radius, FN=FN);
    
//cylinder(r1=0, r2=fusiloge_radius, h=fusiloge_nose_length, center = false, $fn=100);

	translate([0, -fusiloge_tail_length-fusiloge_main_length]) mirror([0,0,0]) rotate(a=[-90,0,0]) quad_cone(height=fusiloge_tail_length, radius=fusiloge_radius, FN=FN);

}


module mast()
{
    vertical_airfoil(naca=[0,0,mast_thickness_percent/100], h = mast_height, L = mast_chord);
    
    
}




module front_wing()
{
    // horizontal_airfoil(naca = 0010, h=fw_width, L=fw_chord, aoa=fw_aoa);
     full_wing(span=fw_span, thickness = fw_thickness_percent,chord0 = fw_chord, chordf=fw_chord/10, curve=fw_curve, dz=sweep_dz);
}

module back_wing()
{
	// center at center of wing, leading edge
    rotate([bw_aoa, 0, 0]) full_wing(span=bw_span, thickness=bw_thickness_percent, chord0 = bw_chord, chordf=bw_chord/10, curve=bw_curve, dz=sweep_dz);
}


module horizontal_airfoil(naca=0012, L=100, N=81, h=1, aoa=0, open=false)
{ 
  translate([-h/2, 0, 0]) rotate([0, 90, 0]) linear_extrude(height = h)  rotate(-90+aoa) polygon(points = airfoil_data(naca, L, N, open)); 
};


module vertical_airfoil(naca=0012, L = 100, N = 81, h = 1, open = false)
{
  linear_extrude(height = h)
  rotate(-90) polygon(points = airfoil_data(naca, L, N, open)); 
};

render_foil(); 
//render_components();


