use <naca/Naca4.scad>
use <naca/Naca_sweep.scad>
use <sweep_wing.scad>
FN = 100;

// parameters
fusiloge_position = [0, 15, 0.25];
fw_position = [0, 15, -1.2]; 
bw_position = [0, -20, 1.4];

mast_height = 100; 
mast_chord = 10;

fusiloge_radius = 1.5;
fusiloge_main_length = 40;
fusiloge_nose_length = 5;
fusiloge_tail_length = 10;

fw_span = 60.0; 
fw_chord = 10;
fw_aoa = 0;
fw_curve = 5;

bw_span = 30.0;
bw_chord = 6;
bw_aoa = -2;
bw_curve = -4;

sweep_dz = .5;


module render_foil()
{
    union()
    {
   translate(fusiloge_position) fusiloge();
	mast();
   translate(fw_position) 	front_wing();
   translate(bw_position)    back_wing();
    }
};


module render_fusiloge()
{ 
    
	difference() {
		translate(fusiloge_position) fusiloge();
	    mast();
		translate(fw_position) front_wing();
		translate(bw_position) back_wing();
        };
        
};
	

module render_front_wing()
{ 
	translate(fw_position) front_wing();
};

module render_back_wing()
{ 
	translate(bw_position) back_wing();
};



module quadcone()
{
    function fquartic(x,y) = x*x+y*y;
    function ftop(x,y) =fquartic(x,y)+5;

};

quadcone();


module fusiloge()
{

	

	rotate(a=[90, 0, 0]) cylinder(r = fusiloge_radius, h = fusiloge_main_length, center = false, $fn=FN);

	
	translate([0, fusiloge_nose_length]) rotate(a=[90, 0, 0]) cylinder(r1=0, r2=fusiloge_radius, h=fusiloge_nose_length, center = false, $fn=100);

	translate([0, -fusiloge_main_length]) rotate(a=[90, 0, 0]) cylinder(r1=fusiloge_radius, r2=0, h=fusiloge_tail_length, center = false, $fn=100);

}


module mast()
{
    vertical_airfoil(naca=0020, h = mast_height, L = mast_chord);
}




module front_wing()
{
    // horizontal_airfoil(naca = 0010, h=fw_width, L=fw_chord, aoa=fw_aoa);
     full_wing(span=fw_span, chord0 = fw_chord, chordf=fw_chord/10, curve=fw_curve, dz=sweep_dz);
}

module back_wing()
{
	// center at center of wing, leading edge
    rotate([bw_aoa, 0, 0]) full_wing(span=bw_span, chord0 = bw_chord, chordf=bw_chord/10, curve=bw_curve, dz=sweep_dz);
}


module horizontal_airfoil(naca=0012, L=100, N=81, h=1, aoa=0, open=false)
{ 
  translate([-h/2, 0, 0]) rotate([0, 90, 0]) linear_extrude(height = h)  rotate(-90+aoa) polygon(points = airfoil_data(naca, L, N, open)); 
};


module vertical_airfoil(naca=0012, L = 100, N = 81, h = 1, open = false)
{
  linear_extrude(height = h)
  rotate(-90) polygon(points = airfoil_data(naca, L, N, open)); 
};

render_foil(); 

//translate([100, 0]) render_fusiloge();
//translate([100, 0, -5]) render_front_wing();
//translate([100, 0, 5]) render_back_wing();
//translate([100, 0, 5]) mast();

